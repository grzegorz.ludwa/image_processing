from queue import Empty, Queue
from threading import Event, Thread
from typing import Callable

import numpy


class ImageProcessing(Thread):
    """Process images with provided functions"""

    def __init__(
        self, stop_event: Event, input_queue: Queue, output_queue: Queue
    ) -> None:
        Thread.__init__(self)
        self.stopped = stop_event
        self.input_queue = input_queue
        self.output_queue = output_queue

        self._process_functions = []

    def run(self) -> None:
        """Thread main function"""
        while not self.stopped.is_set():
            try:
                image = self.input_queue.get(timeout=0.2)
            except Empty:
                pass
            else:
                self.output_queue.put(self._process(image))

    def _process(self, image: numpy.ndarray) -> numpy.ndarray:
        """Process image with all functions"""
        for fun in self._process_functions:
            image = fun(image)

        return image

    def register_function(
        self, function: Callable[[numpy.ndarray], numpy.ndarray]
    ) -> None:
        """Register new function at the end of processing chain"""
        # TODO: Consider locking this during execution?
        self._process_functions.append(function)
