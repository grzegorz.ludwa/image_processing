from queue import Queue
from threading import Event, Thread

from image_processing.source import Source


class ImageProducer(Thread):
    """Produce images with specified time interval until it's canceled"""

    def __init__(
        self, stop_event: Event, source: Source, output_queue: Queue, interval: float
    ) -> None:
        Thread.__init__(self)
        self.stopped = stop_event
        self.source = source
        self.output_queue = output_queue
        self.interval = interval

    def run(self) -> None:
        """Thread main function"""
        while not self.stopped.wait(self.interval):
            self.output_queue.put(self.source.get_data())
