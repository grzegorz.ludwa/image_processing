from pathlib import Path
from queue import Empty, Queue
import tempfile
from threading import Event

import cv2

from image_processing.consumer import ImageProcessing
from image_processing.producer import ImageProducer
from image_processing.source import Source


PROCESSED_FRAMES_DIRECTORY = Path("processed")
MAX_PROCESSED_FRAMES_COUNT = 100


def create_output_directory() -> Path:
    """Create output directory with random name for processed frames"""
    PROCESSED_FRAMES_DIRECTORY.mkdir(parents=True, exist_ok=True)
    return Path(tempfile.mkdtemp(dir=PROCESSED_FRAMES_DIRECTORY.resolve()))


def main():
    print("Running image processing demo...")
    # TODO: Introduce logging in whole app

    source_shape = (1024, 768, 3)
    source = Source(source_shape)

    stop_threads = Event()
    input_image_queue = Queue()
    output_image_queue = Queue()
    producer = ImageProducer(
        stop_event=stop_threads,
        source=source,
        output_queue=input_image_queue,
        interval=0.02,
    )
    consumer = ImageProcessing(
        stop_event=stop_threads,
        input_queue=input_image_queue,
        output_queue=output_image_queue,
    )
    consumer.register_function(
        lambda frame: cv2.resize(
            frame, dsize=(int(source_shape[0] / 2), int(source_shape[1] / 2))
        )
    )
    consumer.register_function(lambda frame: cv2.medianBlur(frame, 5))

    print("Starting image producer and consumer threads...")
    consumer.start()
    producer.start()

    processed_tempdir = create_output_directory()
    processed_frames_count = 0
    running = True
    print(
        "Running program main loop. "
        f"Processed files will be saved into: {processed_tempdir.resolve()}"
    )
    print(f"Press Ctrl+C to stop.")
    while running and processed_frames_count < MAX_PROCESSED_FRAMES_COUNT:
        try:
            frame = output_image_queue.get(timeout=0.2)
            filename = processed_tempdir / f"frame_{processed_frames_count}.png"
            cv2.imwrite(str(filename), frame)
            processed_frames_count += 1
        except Empty:
            ...
        except KeyboardInterrupt:
            running = False

    stop_threads.set()


if __name__ == "__main__":
    main()
