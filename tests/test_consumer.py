from queue import Queue
from threading import Event
import time

import numpy

from image_processing.consumer import ImageProcessing


def test_image_consumer_when_none_function_registered():
    dummy_event = Event()
    dummy_output_queue = Queue()
    dummy_input_queue = Queue()
    dummy_input_queue.put("1")

    sut = ImageProcessing(
        stop_event=dummy_event,
        input_queue=dummy_input_queue,
        output_queue=dummy_output_queue,
    )
    sut.start()
    time.sleep(0.05)
    dummy_event.set()

    assert dummy_input_queue.empty()
    assert dummy_output_queue.qsize() == 1
    assert dummy_output_queue.get_nowait() == "1"

def test_image_consumer_when_processing_functions_registered():
    dummy_event = Event()
    dummy_output_queue = Queue()
    dummy_input_queue = Queue()
    dummy_input_queue.put(numpy.ones((100, 50, 3)))

    sut = ImageProcessing(
        stop_event=dummy_event,
        input_queue=dummy_input_queue,
        output_queue=dummy_output_queue,
    )

    def downsampling(array: numpy.ndarray) -> numpy.ndarray:
        return array[::2, ::2]

    sut.register_function(lambda array: array * 2)
    sut.register_function(downsampling)

    sut.start()
    time.sleep(0.05)
    dummy_event.set()

    assert dummy_input_queue.empty()
    assert dummy_output_queue.qsize() == 1
    output_array = dummy_output_queue.get_nowait()
    assert output_array[0][0][0] == 2
    assert output_array.shape[0] == 50
