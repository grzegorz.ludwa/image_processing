from image_processing.source import Source

def test_get_data():
    source_shape = (100, 50, 3)
    sut = Source(source_shape)

    assert sut.get_data().shape == source_shape
