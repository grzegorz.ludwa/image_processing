from threading import Event
import time
from queue import Queue

from image_processing.producer import ImageProducer
from image_processing.source import Source


def test_image_producer():
    dummy_interval = 0.02
    dummy_queue = Queue()
    dummy_source = Source((100, 50, 1))
    dummy_event = Event()

    sut = ImageProducer(
        stop_event=dummy_event,
        source=dummy_source,
        output_queue=dummy_queue,
        interval=dummy_interval,
    )

    sut.start()
    time.sleep(0.05)
    dummy_event.set()

    # for sure at least 1 elements should be in queue based on intervals
    assert dummy_queue.qsize() >= 1
