# image_processing

Coding task performed December 2022 in context of an interview process.

Copyright (C) 2022 Grzegorz Ludwa

## Prerequisites

- `python` version >=3.8
- `poetry` (`curl -sSL https://install.python-poetry.org | python3 -`)

## Usage

### One time run

1. Clone repository.
2. `cd image_processing`
3. Create virtualenv and install dependencies: `poetry install`.
4. Run app: `poetry run imgproc`

### Install on system

1. Clone repository.
2. `cd image_processing`
3. Build package: `poetry build`.
4. Install on system: `pip install --user dist/image-processing-0.1.0.tar.gz`.


### Run in dev container

TODO


## Excercise description

For the purpose of this task let's assume you have following data source:

``` python
class Source:
    def __init__(self, source_shape: tuple):
        self._source_shape: tuple = source_shape

    def get_data(self) -> np.ndarray:
        rows, cols, channels = self._source_shape
        return np.random.randint(
            256,
            size=rows * cols * channels,
            dtype=np.uint8,
        ).reshape(self._source_shape)
```

Propose an architecture of type Producer-Consumer, which will work as follows:
1. `Producer` (thread) has a data source (image) of size: width=1024 px, height=768 px, channels=3.
Every 50 ms `Producer` get data from `Source` and put them into queue **A**.
2. `Consumer` (also a thread) pull data from queue **A** and do following processing operations:
    - double image size reduction
    - apply median filter with square 5x5 kernel
At the end of processing new image is pushed into queue **B**.

In the main function demonstrate how program works. After processing 100 frames it should stop
automatically. All data from queue **B** should be saved into `processed` directory with `*.png`
extension. It's allowed to use external libraries.
